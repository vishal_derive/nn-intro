# Introduction to Neural Networks using Tensorflow

This repository contains code for a hand-on workshop on Introduction to NN, developed by Vishal Patel (vishal@derive.io).

In this hands-on workshop, I provide a brief introduction to Neural Networks (using slides), including the fundamental concepts and terminology, followed by coding exercises. 

The workshop agenda is as follows:

1. Introduction to Multilayer Perceptron (MLP) _[slides]_ 

2. Develop hand-written digits recognition model _[Jupyter Notebook]_

3. Introduction to Convolutional Neural Networks (CNN) _[slides]_

4. Develop an image classification model using CNN _[Jupyter Notebook]_

5. Image tagging _[Jupyter Notebook]_


## Software

This code repo was developed and tested in Python 3.8. `conda` was used to create virtual enviornment and install dependencies. If `conda` is not installed on your computer, please see **conda installation** section below.

The packages needed to run all the code in this repo are listed in `env.yml`.

Follow these steps to create a stand-alone envirnment named `nn` with Python 3.8 and all the required packages:


**Step 1:** Go to the location (folder), where you would like to store the code and other files for this workshop, and clone the workshop repository from the Bitbucket repo using the following command:

```
git clone git@bitbucket.org:vishal_derive/nn-intro.git
```

This will create a directory (folder) called `nn-intro`. Go to that directory using `cd nn-intro`.

**Step 2:** Create a Python virtual environment (with all required dependencies) called `nn` using conda. (Note: This will take seveal minutes to complete.)

```
$ conda env create --file env.yml -n nn
```

Note that this command also installs all dependencies from `env.yml`.

**Step 3:** Once the environemnt is successfully created, activate it.

```
conda activate nn
```

Now you are ready to run the code for this workshop.

*Note:* Once the workshop is complete, you can deactivate the environment using the following command: `conda deactivate`.

### conda installation

Anaconda can be installed from [here](https://www.anaconda.com/products/individual). 

If you need more detailed instructions, [here's](https://www.datacamp.com/community/tutorials/installing-anaconda-windows) one good resource.

For more information about using conda environments refer to the [Managing Environments](http://conda.pydata.org/docs/using/envs.html) section of the conda documentation.
